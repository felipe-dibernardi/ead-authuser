package com.ead.authuser.controller;

import com.ead.authuser.configs.security.AuthenticationCurrentUserService;
import com.ead.authuser.configs.security.UserDetailsImpl;
import com.ead.authuser.dtos.UserDTO;
import com.ead.authuser.models.UserModel;
import com.ead.authuser.services.UserService;
import com.ead.authuser.specifications.SpecificationTemplate;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Log4j2
@RequiredArgsConstructor
@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    private final AuthenticationCurrentUserService authenticationCurrentUserService;

    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping
    public ResponseEntity<Page<UserModel>> getAllUsers(SpecificationTemplate.UserSpec spec,
                                                       @PageableDefault(page = 0, size = 10, sort = "userId", direction = Sort.Direction.ASC)
                                                               Pageable pageable,
                                                       Authentication authentication) {
        UserDetails userDetails = (UserDetailsImpl) authentication.getPrincipal();
        log.info("Authentication {}", userDetails.getUsername());
        Page<UserModel> userModelPage = userService.findAll(pageable, spec);
        if (!userModelPage.isEmpty()) {
            for (UserModel user : userModelPage.getContent()) {
                user.add(linkTo(methodOn(UserController.class).getOneUser(user.getUserId())).withSelfRel());
            }
        }
        return ResponseEntity.status(HttpStatus.OK).body(userModelPage);
    }

    @PreAuthorize("hasAnyRole('STUDENT')")
    @GetMapping("/{userId}")
    public ResponseEntity<Object> getOneUser(@PathVariable("userId") UUID userId) {
        if (!authenticationCurrentUserService.getCurrentUser().getUserId().equals(userId)) {
            throw new AccessDeniedException("Forbidden");
        }
        Optional<UserModel> userOpt = userService.findById(userId);
        if (userOpt.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(userOpt.get());
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found");
    }

    @DeleteMapping("/{userId}")
    public ResponseEntity<Object> deleteUser(@PathVariable("userId") UUID userId) {
        log.debug("DELETE deleteUser userId received: {} ", userId);
        Optional<UserModel> userOpt = userService.findById(userId);
        if (userOpt.isPresent()) {
            userService.deleteAndPublish(userOpt.get());
            log.debug("DELETE deleteUser userId deleted: {} ", userId);
            log.info("User deleted successfully userId {} ", userId);
            return ResponseEntity.status(HttpStatus.OK).body("User removed successfully");
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found");
    }

    @PutMapping("/{userId}")
    public ResponseEntity<Object> updateUser(@PathVariable("userId") UUID userId,
                                             @RequestBody
                                             @Validated(UserDTO.UserView.UserPut.class)
                                             @JsonView(UserDTO.UserView.UserPut.class) UserDTO userDTO) {
        log.debug("PUT updateUser UserDTO received: {}", userDTO);
        Optional<UserModel> userOpt = userService.findById(userId);
        if (userOpt.isPresent()) {
            UserModel userModel = userOpt.get();
            userModel.setFullName(userDTO.getFullName());
            userModel.setCpf(userDTO.getCpf());
            userModel.setPhoneNumber(userDTO.getPhoneNumber());
            userModel.setLastUpdatedDate(LocalDateTime.now(ZoneId.of("UTC")));
            userService.updateAndPublish(userModel);
            log.debug("PUT updateUser userId saved: {} ", userModel.getUserId());
            log.info("User updated successfully userId {} ", userModel.getUserId());
            return ResponseEntity.status(HttpStatus.OK).body(userModel);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found");
    }

    @PutMapping("/{userId}/password")
    public ResponseEntity<Object> updatePassword(@PathVariable("userId") UUID userId,
                                                 @RequestBody
                                                 @Validated(UserDTO.UserView.PasswordPut.class)
                                                 @JsonView(UserDTO.UserView.PasswordPut.class) UserDTO userDTO) {
        log.debug("PUT updateUser UserDTO received: {}", userDTO);
        Optional<UserModel> userOpt = userService.findById(userId);
        if (userOpt.isPresent()) {
            UserModel userModel = userOpt.get();
            if (userModel.getPassword().equals(userDTO.getOldPassword())) {
                userModel.setPassword(userDTO.getPassword());
                userModel.setLastUpdatedDate(LocalDateTime.now(ZoneId.of("UTC")));
                userService.updatePassword(userModel);
                log.debug("PUT updatePassword userId saved: {} ", userModel.getUserId());
                log.info("User updated successfully userId {} ", userModel.getUserId());
                return ResponseEntity.status(HttpStatus.OK).body("Password updated successfully");
            } else {
                log.warn("Mismatched old password userId {}", userDTO.getUserId());
                return ResponseEntity.status(HttpStatus.CONFLICT).body("Error: Mismatched old password");
            }
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found");
    }

    @PutMapping("/{userId}/image")
    public ResponseEntity<Object> updateImage(@PathVariable("userId") UUID userId,
                                                 @RequestBody
                                                 @Validated(UserDTO.UserView.ImagePut.class)
                                                 @JsonView(UserDTO.UserView.ImagePut.class) UserDTO userDTO) {
        log.debug("PUT updateImage UserDTO received: {}", userDTO);
        Optional<UserModel> userOpt = userService.findById(userId);
        if (userOpt.isPresent()) {
            UserModel userModel = userOpt.get();
            userModel.setImageUrl(userDTO.getImageUrl());
            userModel.setLastUpdatedDate(LocalDateTime.now(ZoneId.of("UTC")));
            userService.updateAndPublish(userModel);
            log.debug("PUT updateImage userId saved: {} ", userModel.getUserId());
            log.info("User updated successfully userId {} ", userModel.getUserId());
            return ResponseEntity.status(HttpStatus.OK).body(userModel);

        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found");
    }
}
