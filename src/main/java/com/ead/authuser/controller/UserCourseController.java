package com.ead.authuser.controller;

import com.ead.authuser.clients.CourseClient;
import com.ead.authuser.models.UserModel;
import com.ead.authuser.services.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;

@Log4j2
@RequiredArgsConstructor
@RestController
@CrossOrigin(origins = "*", maxAge = 3600L)
public class UserCourseController {

    private final CourseClient courseClient;

    private final UserService userService;

    @PreAuthorize("hasAnyRole('STUDENT')")
    @GetMapping("/users/{userId}/courses")
    public ResponseEntity<Object> getAllCoursesByUser(@PathVariable("userId") UUID userId,
                                                      @PageableDefault(page = 0, size = 10, sort = "courseId",
                                                               direction = Sort.Direction.ASC) Pageable pageable,
                                                      @RequestHeader("Authorization") String token) {
        Optional<UserModel> userOpt = userService.findById(userId);
        if (userOpt.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(courseClient.getAllCoursesByUser(userId, pageable, token));
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found");
    }

}
