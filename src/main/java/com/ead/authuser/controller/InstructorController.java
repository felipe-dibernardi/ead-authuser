package com.ead.authuser.controller;

import com.ead.authuser.dtos.InstructorDTO;
import com.ead.authuser.enums.RoleType;
import com.ead.authuser.enums.UserType;
import com.ead.authuser.models.RoleModel;
import com.ead.authuser.models.UserModel;
import com.ead.authuser.services.RoleService;
import com.ead.authuser.services.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;

@Log4j2
@RequiredArgsConstructor
@RestController
@CrossOrigin(origins = "*", maxAge = 3600L)
@RequestMapping("/instructors")
public class InstructorController {

    private final UserService userService;

    private final RoleService roleService;

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping("/subscription")
    public ResponseEntity<Object> sabeSubscriptionInstructor(@RequestBody @Valid InstructorDTO instructorDTO) {

        Optional<UserModel> userOpt = userService.findById(instructorDTO.getUserId());
        if (userOpt.isPresent()) {
            UserModel userModel = userOpt.get();
            userModel.setUserType(UserType.INSTRUCTOR);
            userModel.setLastUpdatedDate(LocalDateTime.now(ZoneId.of("UTC")));

            RoleModel roleModel = roleService.findByRoleName(RoleType.ROLE_INSTRUCTOR)
                    .orElseThrow(() -> new RuntimeException("Error: Role is Not Found."));

            userModel.getRoles().add(roleModel);

            userService.updateAndPublish(userModel);
            return ResponseEntity.status(HttpStatus.OK).body(userModel);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found");
    }

}
