package com.ead.authuser.clients;

import com.ead.authuser.dtos.CourseDTO;
import com.ead.authuser.dtos.ResponsePageDTO;
import com.ead.authuser.services.UtilsService;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import net.bytebuddy.implementation.bytecode.Throw;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Log4j2
@RequiredArgsConstructor
@Component
public class CourseClient {

    private final RestTemplate restTemplate;

    private final UtilsService utilsService;

    @Value("${ead.api.url.course}")
    private String COURSE_REQUEST_URI;

    //@Retry(name = "retryInstance", fallbackMethod = "retryFallback")
    @CircuitBreaker(name = "circuitbreakerInstance", fallbackMethod = "")
    public Page<CourseDTO> getAllCoursesByUser(UUID userId, Pageable pageable, String token) {
        List<CourseDTO> searchResult = null;
        String url = utilsService.createGetCoursesByUserURL(COURSE_REQUEST_URI, userId, pageable);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", token);
        HttpEntity<String> requestEntity = new HttpEntity<>("parameters", headers);
        log.debug("Request URL: {}", url);
        log.info("Request URL: {}", url);

        ParameterizedTypeReference<ResponsePageDTO<CourseDTO>> responseType =
                    new ParameterizedTypeReference<>() {};
        ResponseEntity<ResponsePageDTO<CourseDTO>> result =
                    restTemplate.exchange(url, HttpMethod.GET, requestEntity, responseType);
        searchResult = result.getBody().getContent();
        log.debug("Response number of Elements: {}", searchResult.size());
        log.info("Ending request /courses userId {} ", userId);
        return new PageImpl<>(searchResult);
    }

    public Page<CourseDTO> circuitbreakerFallback(UUID user, Pageable pageable, Throwable t) {
        log.error("Inside circuitbreaker fallback method, cause - {}", t.toString());
        return new PageImpl<>(new ArrayList<>());
    }

    public Page<CourseDTO> retryFallback(UUID userId, Pageable pageable, Throwable t) {
        log.error("Inside retry fallback method, cause - {}", t.toString());
        return new PageImpl<>(new ArrayList<>());
    }
}
