package com.ead.authuser.services;

import com.ead.authuser.models.BaseModel;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface BaseService<T extends BaseModel> {

    List<T> findAll();

    Optional<T> findById(UUID uuid);

    void delete(T model);

    void save(T model);

}
