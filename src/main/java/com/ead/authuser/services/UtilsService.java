package com.ead.authuser.services;

import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface UtilsService {

    String createGetCoursesByUserURL(String baseURL, UUID userId, Pageable pageable);

    String createDeleteUserInCourses(String baseURL, UUID userId);
}
