package com.ead.authuser.services;

import com.ead.authuser.models.UserModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

public interface UserService extends BaseService<UserModel> {

    Page<UserModel> findAll(Pageable pageable, Specification<UserModel> spec);

    boolean existsByUsername(String username);

    boolean existsByEmail(String email);

    UserModel saveAndPublish(UserModel userModel);

    void deleteAndPublish(UserModel userModel);

    UserModel updateAndPublish(UserModel userModel);

    UserModel updatePassword(UserModel userModel);
}
